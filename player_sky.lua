-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore
    = minetest, nodecore
-- LUALOCALS > ---------------------------------------------------------

nodecore.register_playerstep({
		label = "hypercube sky",
		priority = -1,
		action = function(player, data)
			local dbg = minetest.check_player_privs(player, "debug")
			data.sky = {
				base_color = "#000000",
				type = "plain",
				clouds = false,
				fog = {
					fog_distance = dbg and -1 or 36,
					fog_start = dbg and -1 or 0.95,
				}
			}
			data.daynight = 0
		end
	})
