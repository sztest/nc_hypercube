-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore
    = minetest, nodecore
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local walltxr = "nc_terrain_stone.png^[mask:nc_hypercube_spiral.png"
.. "^(nc_terrain_stone.png^[opacity:192)"

minetest.register_node(modname .. ":wall", {
		description = "Wall",
		tiles = {walltxr},
		sounds = nodecore.sounds("nc_terrain_stony"),
		diggable = false,
	})

minetest.register_node(modname .. ":air", {
		tiles = {"nc_hypercube_spiral.png^[opacity:32"},
		drawtype = "glasslike",
		use_texture_alpha = "blend",
		post_effect_color = "#e0e0e020",
		paramtype = "light",
		light_source = 3,
		pointable = false,
		climbable = true,
		walkable = false,
		diggable = false,
		groups = {
			[modname .. "_compass"] = 1,
			support_falling = 1,
		},
	})

minetest.register_node(modname .. ":light", {
		description = "Light",
		tiles = {walltxr .. "^nc_hypercube_light.png"},
		sounds = nodecore.sounds("nc_terrain_stony"),
		paramtype = "light",
		light_source = 14,
		diggable = false,
	})

minetest.register_node(modname .. ":beam", {
		drawtype = "airlike",
		air_equivalent = true,
		sunlight_propagates = true,
		paramtype = "light",
		light_source = 14,
		diggable = false,
		buildable_to = true,
		pointable = false,
		walkable = false,
	})

minetest.register_node(modname .. ":portal", {
		description = "Portal",
		drawtype = "glasslike",
		tiles = {
			{
				name = "nc_hypercube_noise.png^[multiply:#404040",
				animation = {
					["type"] = "vertical_frames",
					aspect_w = 16,
					aspect_h = 16,
					length = 16/60
				}
			}
		},
		groups = {
			[modname .. "_portal"] = 1,
			support_falling = 1
		},
		post_effect_color = "#000000ff",
		climbable = true,
		walkable = false,
		diggable = false,
	})

minetest.register_node(modname .. ":portal_back", {
		description = "Portal",
		drawtype = "glasslike",
		tiles = {
			{
				name = "nc_hypercube_noise.png^[invert:rgb^[multiply:#404040",
				animation = {
					["type"] = "vertical_frames",
					aspect_w = 16,
					aspect_h = 16,
					length = 16/60
				}
			}
		},
		groups = {[modname .. "_portal"] = 1},
		post_effect_color = "#000000ff",
		climbable = true,
		walkable = false,
		diggable = false,
	})
