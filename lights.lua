-- LUALOCALS < ---------------------------------------------------------
local VoxelArea, math, minetest, nodecore, pairs, vector
    = VoxelArea, math, minetest, nodecore, pairs, vector
local math_floor
    = math.floor
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local c_air = minetest.get_content_id("air")
local c_beam = minetest.get_content_id(modname .. ":beam")
local propagates = {}
local airequiv = {}
minetest.after(0, function()
		for k, v in pairs(minetest.registered_nodes) do
			local c = minetest.get_content_id(k)
			if c ~= c_beam and v.air_equivalent then
				airequiv[k] = true
				airequiv[c] = true
			end
			if v.sunlight_propagates then
				propagates[k] = true
				propagates[c] = true
			end
		end
	end)

local function getvm(...)
	local vm = minetest.get_voxel_manip(...)
	local data = vm:get_data()
	local emin, emax = vm:get_emerged_area()
	local area = VoxelArea(emin, emax)
	return vm, data, area
end

local radius = 13
local function fixlight(bpos, lpos)
	local minp = vector.new(bpos.x - radius, bpos.y, bpos.z - radius)
	local maxp = vector.new(lpos.x + radius, lpos.y, lpos.z + radius)
	local vm, data, area = getvm(minp, maxp)
	local beam = true
	for y = lpos.y - 1, bpos.y, -1 do
		local offs = area:index(lpos.x, y, lpos.z)
		local d = data[offs]
		if beam then
			if not propagates[d] then
				beam = false
			elseif airequiv[d] then
				data[offs] = c_beam
			end
		elseif d == c_beam then
			data[offs] = c_air
		end
	end
	vm:set_data(data)
	vm:write_to_map()
	return true
end

local function checklight(pos)
	local ly = math_floor(pos.y / 32) * 32 + 26
	local lpos = vector.new(pos.x, ly, pos.z)
	local lnode = minetest.get_node(lpos)
	if lnode.name ~= modname .. ":light" then
		return false
	end

	local by = ly - 20
	local bpos = vector.new(pos.x, by, pos.z)
	local beam = true
	local cpos = vector.new(pos.x, ly, pos.z)
	for y = ly - 1, by, -1 do
		cpos.y = y
		local nn = minetest.get_node(cpos).name
		if beam then
			if not propagates[nn] then
				beam = false
			elseif airequiv[nn] then
				return fixlight(bpos, lpos)
			end
		elseif nn == modname .. ":beam" then
			return fixlight(bpos, lpos)
		end
	end
end

minetest.register_abm({
		label = modname .. " lighting check",
		nodenames = {modname .. ":light"},
		interval = 1,
		chance = 10,
		action = checklight
	})
minetest.register_abm({
		label = modname .. " beam check",
		nodenames = {modname .. ":beam"},
		interval = 1,
		chance = 10,
		action = function(pos)
			if checklight(pos) == false then
				return minetest.remove_node(pos)
			end
		end
	})

nodecore.register_on_nodeupdate(checklight)
