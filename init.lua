-- LUALOCALS < ---------------------------------------------------------
local include, minetest, rawset
    = include, minetest, rawset
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = {}
rawset(_G, modname, api)

include("math")
include("materials")
include("schem_api")
include("schem_world")
include("schem_content")
include("mapgen")
include("lights")
include("player_compass")
include("player_portal")
include("player_preload")
include("player_sky")
include("item_ent")
include("sunlight")
