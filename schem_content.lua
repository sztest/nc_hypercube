-- LUALOCALS < ---------------------------------------------------------
local ipairs, minetest, nodecore, vector
    = ipairs, minetest, nodecore, vector
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

local create = api.schem.create
local get = api.schem.get
local set = api.schem.set
local fill = api.schem.fill
local copy = api.schem.copy

------------------------------------------------------------------------

api.roomcontent = {}

------------------------------------------------------------------------

do
	local s = create(20, 21, 20)
	local hlite = {name = modname .. ":light"}
	local hbeam = {name = modname .. ":beam"}
	local lightoffs = {0, 5, 9, 10, 14, 19}
	for _, x in ipairs(lightoffs) do
		for _, z in ipairs(lightoffs) do
			if x < 9 or x > 10 or z < 9 or z > 10 then
				set(s, x, 20, z, hlite)
				fill(s, x, 0, z, x, 19, z, hbeam)
			end
		end
	end
	fill(s, 0, 0, 0, 19, 2, 19, {name = "nc_terrain:dirt"})
	fill(s, 0, 3, 0, 19, 3, 19, {name = "nc_terrain:dirt_with_grass"})
	local plain = minetest.register_schematic(s)
	local function sparse(vm, pos)
		minetest.place_schematic_on_vmanip(vm, pos, plain)
		for dx = 4, 14, 10 do
			for dz = 4, 14, 10 do
				minetest.place_schematic_on_vmanip(vm,
					vector.offset(pos, dx, 3, dz),
					nodecore.tree_schematic,
					nil, nil, nil, "place_center_x,place_center_z")
			end
		end
	end
	for _ = 1, 10 do api.roomcontent[#api.roomcontent + 1] = sparse end
	local function dense(vm, pos)
		minetest.place_schematic_on_vmanip(vm, pos, plain)
		for dx = 2, 17, 5 do
			for dz = 2, 17, 5 do
				minetest.place_schematic_on_vmanip(vm,
					vector.offset(pos, dx, 3, dz),
					nodecore.tree_schematic,
					nil, nil, nil, "place_center_x,place_center_z")
			end
		end
	end
	for _ = 1, 5 do api.roomcontent[#api.roomcontent + 1] = dense end
end
