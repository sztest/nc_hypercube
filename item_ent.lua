-- LUALOCALS < ---------------------------------------------------------
local math, nodecore, vector
    = math, nodecore, vector
local math_floor
    = math.floor
-- LUALOCALS > ---------------------------------------------------------

nodecore.register_item_entity_step(function(self, dtime)
		local pos = self.object:get_pos()
		if not pos then return end
		local relpos = {
			x = pos.x - math_floor(pos.x / 32) * 32 - 15.5,
			y = pos.y - math_floor(pos.y / 32) * 32 - 15.5,
			z = pos.z - math_floor(pos.z / 32) * 32 - 15.5,
		}
		if relpos.x < -10.5 or relpos.x > 10.5
		or relpos.y < -10.5 or relpos.y > 10.5
		or relpos.z < -10.5 or relpos.z > 10.5 then
			self.object:add_velocity(vector.multiply(relpos, -dtime))
		end
	end)
