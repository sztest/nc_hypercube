-- LUALOCALS < ---------------------------------------------------------
local math, minetest, vector
    = math, minetest, vector
local math_abs, math_floor
    = math.abs, math.floor
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

local pos_base = -29984
local pos_limit = 1874

-- Maximum length of a path, in rooms after the
-- origin room.
local max_depth = 12
api.path_max_depth = max_depth

-- Valid directions.
local dirs = {
	vector.new(0, 1, 0), -- 1 = up
	vector.new(0, 0, 1), -- 2 = north
	vector.new(0, 0, -1), -- 3 = south
	vector.new(1, 0, 0), -- 4 = east
	vector.new(-1, 0, 0), -- 5 = west
	vector.new(0, -1, 0), -- 6 = down
}
api.dirs = dirs

local dir_opp = {6, 3, 2, 5, 4, 1}
api.dir_opp = function(x) return dir_opp[x] end

-- Convert a relative position into a direction number
local function pos_to_dir(pos)
	if math_abs(pos.y) >= math_abs(pos.x) then
		if math_abs(pos.y) >= math_abs(pos.z) then
			return pos.y >= 0 and 1 or 6
		end
		return pos.z >= 0 and 2 or 3
	end
	if math_abs(pos.z) >= math_abs(pos.x) then
		return pos.z >= 0 and 2 or 3
	end
	return pos.x >= 0 and 4 or 5
end
api.pos_to_dir = pos_to_dir

-- Going "backwards" undoes a path component rather than
-- adding another one, so there are only 5 forward and 1
-- backward directions in each room.

local function path_opt(path)
	local t = {}
	for i = 1, #path do
		if t[#t] == dir_opp[path[i]] then
			t[#t] = nil
		else
			t[#t + 1] = path[i]
		end
	end
	return t
end
api.path_opt = path_opt

-- Each path component only has 5 degrees of freedom, since
-- traveling backwards shortens the path instead of adding
-- a component, so we can encode each path in a "base 5"
-- system, with each component being the path direction minus
-- the backward value of the previous path (so it's always 1
-- to 5) minus 1.

-- This system doesn't seem quite right (we should be able
-- to pack valid numerical values much more densely than the
-- ~4% we got) but it succesfully tested as bijective, and
-- it's good enough for "game" purposes.

local function path_to_idx(path)
	path = path_opt(path)
	if #path < 1 then return 0 end
	local idx = 1
	for i = 1, #path do
		local n = path[i] - (i > 1 and dir_opp[path[i - 1]] or 0) - 1
		if n < 0 then n = n + 6 end
		idx = idx * 5 + n
	end
	return idx
end
api.path_to_idx = path_to_idx

local function idx_to_path(idx)
	local path = {}
	while idx >= 5 do
		path[#path + 1] = idx % 5
		idx = math_floor(idx / 5)
	end
	for i = 1, #path / 2 do
		local j = #path - i + 1
		local tmp = path[i]
		path[i] = path[j]
		path[j] = tmp
	end
	path[1] = path[1] and (path[1] + 1)
	for i = 2, #path do
		path[i] = (path[i] + dir_opp[path[i - 1]]) % 6 + 1
	end
	return path
end
api.idx_to_path = idx_to_path

-- Not every index represents a valid path; test whether
-- a given index is valid or not; used by mapgen to fill in
-- invalid rooms.

local function idx_validate(idx)
	local path = idx_to_path(idx)
	if #path > max_depth then return end
	for i = 1, #path do
		if path[i] < 1 or path[i] > #dirs then return end
	end
	return idx == path_to_idx(path)
end
api.idx_validate = idx_validate

-- Convert a room index to a position and vice versa by packing
-- index values along the 3 axes.

local function idx_to_pos(idx)
	local x = idx % pos_limit
	idx = (idx - x) / pos_limit
	local y = idx % pos_limit
	local z = (idx - y) / pos_limit
	return vector.new(
		x * 32 + pos_base,
		y * 32 + pos_base,
		z * 32 + pos_base
	)
end
api.idx_to_pos = idx_to_pos

local function pos_to_idx(pos)
	local x = math_floor((pos.x - pos_base) / 32)
	if x < 0 or x >= pos_limit then return end
	local y = math_floor((pos.y - pos_base) / 32)
	if y < 0 or y >= pos_limit then return end
	local z = math_floor((pos.z - pos_base) / 32)
	if z < 0 or z >= pos_limit then return end
	return x + (y + (z * pos_limit)) * pos_limit
end
api.pos_to_idx = pos_to_idx
