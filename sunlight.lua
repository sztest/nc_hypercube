-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore
    = minetest, nodecore
-- LUALOCALS > ---------------------------------------------------------

-- There is never any access to the "real" sun, so allow
-- artificial light to serve its purpose.

function nodecore.get_depth_light()
	return 0
end

function nodecore.is_full_sun(pos)
	return minetest.get_node_light(pos, 0.5) >= 11
end
