-- LUALOCALS < ---------------------------------------------------------
local minetest, pairs, vector
    = minetest, pairs, vector
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

local function get(schem, x, y, z)
	return schem.data[x + y * schem.size.x + z * schem.size.x * schem.size.y]
end
local function set(schem, x, y, z, node)
	schem.data[x + y * schem.size.x + z * schem.size.x * schem.size.y] = node
end

local void = {name = "air", prob = 0}
local function fill(schem, x1, y1, z1, x2, y2, z2, node)
	node = node or void
	for x = x1, x2 do
		for y = y1, y2 do
			for z = z1, z2 do
				set(schem, x, y, z, node)
			end
		end
	end
end

local function create(x, y, z)
	local schem = {
		size = vector.new(x, y, z),
		data = {},
	}
	fill(schem, 0, 0, 0, x - 1, y - 1, z - 1)
	return schem
end

local function copy(schem)
	local d = {}
	for k, v in pairs(schem.data) do d[k] = v end
	return {
		size = schem.size,
		data = d
	}
end

api.schem = {
	create = create,
	get = get,
	set = set,
	fill = fill,
	copy = copy
}
