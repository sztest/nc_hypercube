-- LUALOCALS < ---------------------------------------------------------
local math, minetest, nodecore, vector
    = math, minetest, nodecore, vector
local math_floor
    = math.floor
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

minetest.set_mapgen_setting("mgname", "singlenode", true)

local content = api.roomcontent

local mapperlin
minetest.after(0, function() mapperlin = minetest.get_perlin(0, 1, 0, 1) end)

local function mkroom(pos, vm)
	local schem = api.schem_solid
	local idx = api.pos_to_idx(pos)
	if idx and api.idx_validate(idx) then
		if idx == 0 then
			schem = api.schem_room_start
		else
			local path = api.idx_to_path(idx)
			local dir = path[#path]
			if #path >= api.path_max_depth then
				schem = api.schem_room_terminal[dir]
			else
				schem = api.schem_room_from[dir]
			end
		end
	end
	minetest.place_schematic_on_vmanip(vm, pos, schem)
	if schem ~= api.schem_solid and idx ~= 0 then
		local rng = nodecore.seeded_rng(mapperlin:get_3d(pos))
		local pick = content[rng(1, #content)]
		pick(vm, vector.offset(pos, 6, 6, 6), rng)
	end
end

minetest.register_on_generated(function(minp, maxp)
		local vm = minetest.get_mapgen_object("voxelmanip")
		local rmin = vector.new(
			math_floor(minp.x / 32) * 32,
			math_floor(minp.y / 32) * 32,
			math_floor(minp.z / 32) * 32
		)
		local rmax = vector.new(
			math_floor(maxp.x / 32) * 32,
			math_floor(maxp.y / 32) * 32,
			math_floor(maxp.z / 32) * 32
		)
		for z = rmin.z, rmax.z, 32 do
			for y = rmin.y, rmax.y, 32 do
				for x = rmin.x, rmax.x, 32 do
					mkroom(vector.new(x, y, z), vm)
				end
			end
		end
		vm:calc_lighting()
		vm:write_to_map()
	end)

local startidx = api.path_to_idx({})
local startpos = api.idx_to_pos(startidx)
startpos = vector.offset(startpos, 15.5, 5.5, 15.5)
minetest.register_on_newplayer(function(player)
		player:set_pos(startpos)
	end)
