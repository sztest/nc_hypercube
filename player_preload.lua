-- LUALOCALS < ---------------------------------------------------------
local math, minetest, nodecore, pairs, string, vector
    = math, minetest, nodecore, pairs, string, vector
local math_floor, string_format
    = math.floor, string.format
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

local relblocks = {
	vector.new(0, 0, 0),
	vector.new(1, 0, 0),
	vector.new(1, 1, 0),
	vector.new(0, 1, 0),
	vector.new(0, 1, 1),
	vector.new(0, 0, 1),
	vector.new(1, 0, 1),
	vector.new(1, 1, 1),
}

local psendq = {}
minetest.register_globalstep(function()
		local del = {}
		for k, v in pairs(psendq) do
			local player = minetest.get_player_by_name(k)
			if not player then
				del[#del + 1] = k
			else
				local bpos = v[#v]
				v[#v] = nil
				if #v < 1 then del[#del + 1] = k end
				local qty = 0
				for i = 1, #relblocks do
					if player:send_mapblock(vector.add(bpos, relblocks[i])) then
						qty = qty + 1
					end
				end
				local pos = vector.multiply(bpos, 16)
				nodecore.log("info", string_format(
						"present %d blocks of %s to %s",
						qty, minetest.pos_to_string(pos), k))
			end
		end
		for i = 1, #del do psendq[del[i]] = nil end
	end)

local pending = 0

local sendto = {}
local function loadsend(player, basepos)
	local pname = player:get_player_name()
	local key = minetest.hash_node_position(basepos)
	local found = sendto[key]
	if not found then
		found = {}
		sendto[key] = found
		pending = pending + 1
		nodecore.log("info", string_format("preload of %s started by %s, pending %d",
				minetest.pos_to_string(basepos), pname, pending))
		minetest.emerge_area(basepos, vector.offset(basepos, 31, 31, 31),
			function(_, _, remain)
				if remain > 0 then return end
				pending = pending - 1
				nodecore.log("info", string_format("preload of %s finished, pending %d",
						minetest.pos_to_string(basepos), pending))
				local bpos = vector.new(
					math_floor(basepos.x / 16),
					math_floor(basepos.y / 16),
					math_floor(basepos.z / 16))
				for name in pairs(found) do
					local pq = psendq[name]
					if not pq then
						pq = {}
						psendq[name] = pq
					end
					pq[#pq + 1] = bpos
				end
				sendto[key] = nil
			end)
	end
	found[pname] = true
end

nodecore.register_playerstep({
		label = "preload through portals",
		action = function(player, data, dtime)
			local ppos = player:get_pos()
			local idx = api.pos_to_idx(ppos)

			if data.hyperidx == idx then return end
			if data.hyperidx_pend == idx then
				data.hyperidx_ptime = data.hyperidx_ptime - dtime
				if data.hyperidx_ptime > 0 then return end
			else
				data.hyperidx_pend = idx
				data.hyperidx_ptime = 2
			end
			data.hyperidx = idx

			local path = idx == 0 and {} or api.idx_to_path(idx)
			local pnew = #path + 1

			for dir = 1, (idx == 0 and 5 or 6) do
				path[pnew] = dir
				local nidx = api.path_to_idx(path)
				if api.idx_validate(nidx) then
					loadsend(player, api.idx_to_pos(nidx))
				end
			end
		end
	})
