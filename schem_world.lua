-- LUALOCALS < ---------------------------------------------------------
local minetest, vector
    = minetest, vector
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

local create = api.schem.create
local get = api.schem.get
local set = api.schem.set
local fill = api.schem.fill
local copy = api.schem.copy

------------------------------------------------------------------------

local base = create(32, 32, 32)

local hwall = {name = modname .. ":wall"}

-- Solid cube

fill(base, 0, 0, 0, 31, 31, 31, hwall)

api.schem_solid = minetest.register_schematic(base)

local hair = {name = modname .. ":air"}
local hportal = {name = modname .. ":portal"}

-- Outer hallways & portals

fill(base, 1, 3, 3, 30, 4, 4, hportal)
fill(base, 1, 3, 27, 30, 4, 28, hportal)
fill(base, 1, 27, 27, 30, 28, 28, hportal)
fill(base, 1, 27, 3, 30, 28, 4, hportal)
fill(base, 3, 3, 3, 28, 4, 4, hair)
fill(base, 3, 3, 27, 28, 4, 28, hair)
fill(base, 3, 27, 27, 28, 28, 28, hair)
fill(base, 3, 27, 3, 28, 28, 4, hair)

fill(base, 3, 1, 3, 4, 30, 4, hportal)
fill(base, 27, 1, 3, 28, 30, 4, hportal)
fill(base, 27, 1, 27, 28, 30, 28, hportal)
fill(base, 3, 1, 27, 4, 30, 28, hportal)
fill(base, 3, 3, 3, 4, 28, 4, hair)
fill(base, 27, 3, 3, 28, 28, 4, hair)
fill(base, 27, 3, 27, 28, 28, 28, hair)
fill(base, 3, 3, 27, 4, 28, 28, hair)

fill(base, 3, 3, 1, 4, 4, 30, hportal)
fill(base, 3, 27, 1, 4, 28, 30, hportal)
fill(base, 27, 27, 1, 28, 28, 30, hportal)
fill(base, 27, 3, 1, 28, 4, 30, hportal)
fill(base, 3, 3, 3, 4, 4, 28, hair)
fill(base, 3, 27, 3, 4, 28, 28, hair)
fill(base, 27, 27, 3, 28, 28, 28, hair)
fill(base, 27, 3, 3, 28, 4, 28, hair)

-- Cross hallways

fill(base, 3, 15, 3, 28, 16, 4, hair)
fill(base, 3, 15, 27, 28, 16, 28, hair)
fill(base, 3, 15, 3, 4, 16, 28, hair)
fill(base, 27, 15, 3, 28, 16, 28, hair)

fill(base, 15, 3, 3, 16, 4, 28, hair)
fill(base, 15, 27, 3, 16, 28, 28, hair)
fill(base, 15, 3, 3, 16, 28, 4, hair)
fill(base, 15, 3, 27, 16, 28, 28, hair)

fill(base, 3, 3, 15, 4, 28, 16, hair)
fill(base, 27, 3, 15, 28, 28, 16, hair)
fill(base, 3, 3, 15, 28, 4, 16, hair)
fill(base, 3, 27, 15, 28, 28, 16, hair)

-- Inner openings

fill(base, 15, 15, 3, 16, 16, 28, hair)
fill(base, 15, 3, 15, 16, 28, 16, hair)
fill(base, 3, 15, 15, 28, 16, 16, hair)

local air = {name = "air"}

-- Inner open space

fill(base, 6, 6, 6, 25, 25, 25, air)

------------------------------------------------------------------------

local start = copy(base)

-- Start room has no "down" exit

fill(start, 0, 0, 0, 31, 2, 31, hwall)

local hlite = {name = modname .. ":light"}
local hbeam = {name = modname .. ":beam"}

-- Ceiling lights

for x = 11, 21, 10 do
	for z = 11, 21, 10 do
		set(start, x, 26, z, hlite)
		fill(start, x, 6, z, x, 25, z, hbeam)
	end
end

api.schem_room_start = minetest.register_schematic(start)

-- Rooms with "back" portals, with or without forward exits

local hback = {name = modname .. ":portal_back"}
api.schem_room_from = {}
api.schem_room_terminal = {}
for dir = 1, #api.dirs do
	local b = copy(base)
	local t = copy(base)
	for x = 0, 31 do
		for y = 0, 31 do
			for z = 0, 31 do
				if get(b, x, y, z) == hportal then
					if api.pos_to_dir(vector.new(x - 15.5, y - 15.5, z - 15.5))
					== api.dir_opp(dir) then
						set(b, x, y, z, hback)
						set(t, x, y, z, hback)
					else
						set(t, x, y, z, hwall)
					end
				end
			end
		end
	end
	api.schem_room_from[dir] = minetest.register_schematic(b)
	api.schem_room_terminal[dir] = minetest.register_schematic(t)
end
