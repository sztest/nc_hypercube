-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore, vector
    = minetest, nodecore, vector
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local compass = nodecore.group_expand("group:" .. modname .. "_compass", true)

nodecore.register_playerstep({
		label = "hypercube sky",
		priority = -1,
		action = function(player)
			local ppos = player:get_pos()
			local eyepos = vector.offset(ppos, 0,
				player:get_properties().eye_height, 0)
			local nn = minetest.get_node(eyepos).name
			if compass[nn] then
				return nodecore.hud_set(player, {
						label = "compass",
						hud_elem_type = "compass",
						size = {x = 48, y = 48},
						text = "nc_hypercube_compass.png^[opacity:64",
						position = {x = 0.5, y = 0.5},
						alignment = {x = 0, y = 0},
						offset = {x = 0, y = 0},
						direction = 1
					})
			else
				return nodecore.hud_set(player, {
						label = "compass",
						ttl = 0
					})
			end
		end
	})
