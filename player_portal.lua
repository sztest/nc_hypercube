-- LUALOCALS < ---------------------------------------------------------
local minetest, nodecore, vector
    = minetest, nodecore, vector
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local api = _G[modname]

local portals = nodecore.group_expand("group:" .. modname .. "_portal", true)

nodecore.register_playerstep({
		label = "hypercube portals",
		action = function(player)
			local ppos = player:get_pos()
			local eyepos = vector.offset(ppos, 0,
				player:get_properties().eye_height, 0)

			local nn = minetest.get_node(eyepos).name
			if not portals[nn] then return end

			local idx = api.pos_to_idx(ppos)
			local path = idx == 0 and {} or api.idx_to_path(idx)
			local basepos = api.idx_to_pos(idx)
			local cpos = vector.offset(basepos, 15.5, 15.5, 15.5)
			local pdir = api.pos_to_dir(vector.subtract(eyepos, cpos))
			path[#path + 1] = pdir
			path = api.path_opt(path)
			if #path > api.path_max_depth then return end

			local tidx = api.path_to_idx(path)
			local tpos = api.idx_to_pos(tidx)
			tpos = vector.add(tpos, vector.subtract(ppos, basepos))
			tpos = vector.add(tpos, vector.multiply(api.dirs[pdir], -26))
			player:set_pos(tpos)
		end
	})
